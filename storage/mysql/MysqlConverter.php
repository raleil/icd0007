<?php
namespace APP\storage\mysql;

use APP\model\Author;
use APP\model\Book;
use APP\storage\Converter;

class MysqlConverter implements Converter
{
    public static function dataToBook(array $data): Book
    {
        $book = new Book();

        $book->setID($data['bookID']);
        $book->setTitle($data['bookTitle']);
        $book->setGrade($data['bookGrade']);
        $book->setIsRead($data['bookIsRead']);

        return $book;
    }

    public static function dataToAuthor(array $data): Author
    {
        $author = new Author();

        $author->setID($data['authorID']);
        $author->setFirstName($data['authorFirstName']);
        $author->setLastName($data['authorLastName']);
        $author->setGrade($data['authorGrade']);

        return $author;
    }

    public static function dataToBookWithAuthor(array $data): Book
    {
        $book = MysqlConverter::dataToBook($data);

        if (isset($data['authorID'])) {
            $book->addAuthor(MysqlConverter::dataToAuthor($data));
        }

        return $book;
    }
}