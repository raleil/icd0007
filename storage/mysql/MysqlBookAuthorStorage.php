<?php
namespace APP\storage\mysql;

use APP\storage\BookAuthorStorage;
use Exception;
use PDO;

class MysqlBookAuthorStorage extends BookAuthorStorage
{
    private PDO $connection;

    public function __construct(Database $database)
    {
        $this->connection = $database->getDB();
        $this->converter = $database->converter;
    }

    public function put(array $data): ?int
    {
        $query = $this->connection->prepare(
            "INSERT INTO book_author (book_id, author_id) VALUES (:bookID, :authorID);"
        );

        $query->bindValue(':bookID', $data['bookID'], PDO::PARAM_INT);
        $query->bindValue(':authorID', $data['authorID'], PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to insert data!");
        }

        return $this->connection->lastInsertId();
    }

    public function getBookWithAuthors(int $id): ?array
    {
        $query = $this->connection->prepare(
            "SELECT book.id AS bookID, book.title AS bookTitle, book.grade AS bookGrade, book.isRead AS bookIsRead, author.id AS authorID, author.firstName AS authorFirstName, author.lastName AS authorLastName, author.grade AS authorGrade
                        FROM book
                                 LEFT JOIN book_author ON book_author.book_id = book.id
                                 LEFT JOIN author ON book_author.author_id = author.id
                        WHERE book.id = :id;"
        );

        $query->bindValue('id', $id, PDO::PARAM_INT);

        $query->execute();

        $results = $query->fetchAll();

        if (count($results) == 0) {
            return null;
        }

        return $results;
    }

    public function getBooksWithAuthors(): ?array
    {
        $query = $this->connection->prepare(
            "SELECT book.id AS bookID, book.title AS bookTitle, book.grade AS bookGrade, book.isRead AS bookIsRead, author.id AS authorID, author.firstName AS authorFirstName, author.lastName AS authorLastName, author.grade AS authorGrade
                        FROM book
                             LEFT JOIN book_author ON book_author.book_id = book.id
                             LEFT JOIN author ON book_author.author_id = author.id"
        );

        $query->execute();

        $results = $query->fetchAll();

        if (count($results) == 0) {
            return null;
        }

        return $results;
    }


    public function deleteByBook(int $id)
    {
        $query = $this->connection->prepare(
            "DELETE FROM book_author WHERE book_id = :id;"
        );

        $query->bindValue('id', $id, PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to delete data");
        }
    }
}