<?php
namespace APP\storage\mysql;

use APP\model\Book;
use APP\storage\BookStorage;
use Exception;
use PDO;

class MysqlBookStorage extends BookStorage
{
    private PDO $connection;

    public function __construct(Database $database)
    {
        $this->connection = $database->getDB();
        $this->converter = $database->converter;
    }

    public function put(Book $book): ?int
    {
        $query = $this->connection->prepare(
            "INSERT INTO book (title, isRead, grade) VALUES (:title, :isRead, :grade);"
        );

        $query->bindValue(':title', $book->getTitle(), PDO::PARAM_STR);
        $query->bindValue(':isRead', $book->getIsRead(), PDO::PARAM_BOOL);
        $query->bindValue(':grade', $book->getGrade(), PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to insert data!");
        }

        return $this->connection->lastInsertId();
    }

    public function get(int $id): ?array
    {
        $query = $this->connection->prepare(
            "SELECT id, title, isRead, grade FROM book WHERE id = :id;"
        );

        $query->bindValue('id', $id);

        $query->execute();

        $results = $query->fetch();

        if (count($results) == 0) {
            return null;
        }

        return $results;
    }

    public function getAll(): array
    {
        $query = $this->connection->prepare(
            "SELECT id, title, isRead, grade FROM book;"
        );

        $query->execute();

        return $query->fetchAll();
    }

    public function update(Book $book)
    {
        $query = $this->connection->prepare(
            "UPDATE book SET title = :title, isRead = :isRead, grade = :grade WHERE id = :id;"
        );

        $query->bindValue(':title', $book->getTitle(), PDO::PARAM_STR);
        $query->bindValue(':isRead', $book->getIsRead(), PDO::PARAM_BOOL);
        $query->bindValue(':grade', $book->getGrade(), PDO::PARAM_INT);
        $query->bindValue(':id', $book->getID(), PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to delete data");
        }
    }

    public function delete(int $id)
    {
        $query = $this->connection->prepare(
            "DELETE FROM book WHERE id = :id;"
        );

        $query->bindValue(':id', $id, PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to delete data");
        }
    }
}