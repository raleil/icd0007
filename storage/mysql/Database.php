<?php
namespace APP\storage\mysql;

use PDO;

class Database
{
    private const USERNAME = 'raleil';
    private const PASSWORD = '89F7';
    private const HOST = 'db.mkalmo.xyz';
    private const DATABASE = 'raleil';

    private static ?Database $database = null;

    private ?PDO $connection;

    public MysqlConverter $converter;

    private function __construct()
    {
        $address = 'mysql:host=' . self::HOST . ';dbname=' . self::DATABASE;
        $this->connection = new PDO($address, self::USERNAME, self::PASSWORD, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

        $this->converter = new MysqlConverter();
    }

    public static function getInstance(): Database
    {
        if (self::$database === null) {
            self::$database = new Database();
        }

        return self::$database;
    }

    public function getDB(): PDO {
        return $this->connection;
    }
}