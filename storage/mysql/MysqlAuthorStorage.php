<?php
namespace APP\storage\mysql;

use APP\model\Author;
use APP\storage\AuthorStorage;
use Exception;
use PDO;

class MysqlAuthorStorage extends AuthorStorage
{
    private PDO $connection;

    public function __construct(Database $database)
    {
        $this->connection = $database->getDB();
        $this->converter = $database->converter;
    }

    public function put(Author $author): ?int
    {
        $query = $this->connection->prepare(
            "INSERT INTO author (firstName, lastName, grade) VALUES (:firstName, :lastName, :grade);"
        );

        $query->bindValue(':firstName', $author->getFirstName(), PDO::PARAM_STR);
        $query->bindValue(':lastName', $author->getLastName(), PDO::PARAM_STR);
        $query->bindValue(':grade', $author->getGrade(), PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to insert data!");
        }

        return $this->connection->lastInsertId();
    }

    public function get(int $id): ?array
    {
        $query = $this->connection->prepare(
            "SELECT id AS authorID, firstName as authorFirstName, lastName AS authorLastName, grade AS authorGrade FROM author WHERE id = :id;"
        );

        $query->bindValue('id', $id);

        $query->execute();

        $result = $query->fetch();

        if (!$result) {
            return null;
        }

        return $result;
    }

    public function getAll(): array
    {
        $query = $this->connection->prepare(
            "SELECT id AS authorID, firstName as authorFirstName, lastName AS authorLastName, grade AS authorGrade FROM author;"
        );

        $query->execute();

        return $query->fetchAll();
    }

    public function update(Author $author)
    {
        $query = $this->connection->prepare(
            "UPDATE author SET firstName = :firstName, lastName = :lastName, grade = :grade WHERE id = :id;"
        );

        $query->bindValue(':firstName', $author->getFirstName(), PDO::PARAM_STR);
        $query->bindValue(':lastName', $author->getLastName(), PDO::PARAM_STR);
        $query->bindValue(':grade', $author->getGrade(), PDO::PARAM_INT);
        $query->bindValue(':id', $author->getID(), PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to delete data");
        }
    }

    public function delete(int $id)
    {
        $query = $this->connection->prepare(
            "DELETE FROM author WHERE id = :id;"
        );

        $query->bindValue(':id', $id, PDO::PARAM_INT);

        if (!$query->execute()) {
            throw new Exception("Failed to delete data");
        }
    }
}