<?php
namespace APP\storage;


use APP\model\Author;

abstract class AuthorStorage
{
    public Converter $converter;

    public function put(Author $author): ?int
    {
        return null;
    }

    public function get(int $id): ?array
    {
        return null;
    }

    public function getAll(): ?array
    {
        return null;
    }

    public function update(Author $author) {}
    public function delete(int $id) {}
}