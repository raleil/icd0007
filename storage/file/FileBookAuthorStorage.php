<?php


namespace APP\storage\file;

use APP\storage\BookAuthorStorage;

class FileBookAuthorStorage extends BookAuthorStorage
{
    private FileBookStorage $bookStorage;
    private FileAuthorStorage $authorStorage;
    private File $bookAuthorFile;

    public function __construct(FileBookStorage $bookStorage, FileAuthorStorage $authorStorage, File $bookAuthorFile)
    {
        $this->bookStorage = $bookStorage;
        $this->authorStorage = $authorStorage;
        $this->bookAuthorFile = $bookAuthorFile;

        $this->converter = $bookAuthorFile->converter;
    }

    public function getBookWithAuthors(int $id): ?array
    {
        $bookAuthors = $this->bookAuthorFile->load();
        $book = $this->bookStorage->get($id);

        $bookWithAuthors = null;

        if ($bookAuthors !== null && count($bookAuthors) > 0) {
            foreach ($bookAuthors as $bookAuthor) {
                if ($bookAuthor['bookID'] == $id) {
                    $author = $this->authorStorage->get($bookAuthor['authorID']);

                    $bookWithAuthors[] = [
                        'bookID' => $book['id'],
                        'book' => $book,
                        'author' => $author
                    ];
                }
            }
        } else {
            $bookWithAuthors[] = [
                'bookID' => $book['id'],
                'book' => $book
            ];
        }

        return $bookWithAuthors;
    }

    public function getBooksWithAuthors(): ?array
    {
        $books = $this->bookStorage->getAll();
        $bookAuthors = $this->bookAuthorFile->load();

        $booksWithAuthors = null;

        if ($books !== null && count($books)) {
            foreach ($books as $book) {

                if ($bookAuthors !== null && count($bookAuthors) > 0) {
                    $hasAuthor = false;

                    foreach ($bookAuthors as $bookAuthor) {
                        if ($book['id'] == $bookAuthor['bookID']) {
                            $hasAuthor = true;
                            $author = $this->authorStorage->get($bookAuthor['authorID']);

                            $booksWithAuthors[] = [
                                'bookID' => $book['id'],
                                'book' => $book,
                                'author' => $author
                            ];
                        }
                    }

                    if (!$hasAuthor) {
                        $booksWithAuthors[] = [
                            'bookID' => $book['id'],
                            'book' => $book
                        ];
                    }
                } else {
                    $booksWithAuthors[] = [
                        'bookID' => $book['id'],
                        'book' => $book
                    ];
                }
            }
        }

        return $booksWithAuthors;
    }

    public function deleteByBook(int $id)
    {
        $bookAuthors = $this->bookAuthorFile->load();

        if ($bookAuthors !== null && count($bookAuthors)) {
            foreach ($bookAuthors as $bookAuthor) {
                if ($bookAuthor['bookID'] == $id) {
                    $this->bookAuthorFile->remove($bookAuthor['id']);
                }
            }
        }
    }

    public function put(array $data): ?int
    {
        return $this->bookAuthorFile->save($data);
    }
}