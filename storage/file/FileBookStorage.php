<?php
namespace APP\storage\file;

use APP\model\Book;
use APP\storage\BookStorage;

class FileBookStorage extends BookStorage
{
    private File $file;

    public function __construct(File $file)
    {
        $this->file = $file;
        $this->converter = $file->converter;
    }

    public function put(Book $book): ?int
    {
        return $this->file->save([
            'id' => $book->getID(),
            'title' => $book->getTitle(),
            'grade' => $book->getGrade(),
            'isRead' => $book->getIsRead()
        ]);
    }

    public function get(int $id): ?array
    {
        $books = $this->file->load();

        foreach ($books as $book) {
            if (isset($book['id']) && $book['id'] == $id) {
                return $book;
            }
        }

        return null;
    }

    public function getAll(): ?array
    {
        return $this->file->load();
    }

    public function delete(int $id)
    {
        $this->file->remove($id);
    }

    public function update(Book $book)
    {
        $this->delete($book->getID());
        $this->put($book);
    }
}