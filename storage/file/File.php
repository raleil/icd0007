<?php
namespace APP\storage\file;

class File
{
    private string $file;

    public FileConverter $converter;

    public function __construct(string $file)
    {
        $this->file = $file;

        $this->converter = new FileConverter();
    }

    public function save(array $data): int
    {
        if (!isset($data['id']) || $data['id'] === null) {
            $data['id'] = $this->getNewID();
        }

        $lineItems = [];

        foreach ($data as $key => $value) {
            $lineItems[] = urlencode($key) . ':' . urlencode($value);
        }

        file_put_contents($this->file, join(';', $lineItems) . PHP_EOL, FILE_APPEND);

        return $data['id'];
    }

    public function load(): ?array
    {
        if (!file_exists($this->file)) {
            return null;
        }

        $lines = file($this->file);

        $fileItems = null;

        foreach ($lines as $line) {
            $item = [];
            $parts = explode(";", trim($line));

            foreach ($parts as $part) {
                $kv = explode(":", trim($part));
                list($key, $value) = $kv;

                $item[urldecode($key)] = urldecode($value);

            }

            $fileItems[] = $item;
        }

        return $fileItems;
    }

    public function remove(int $id)
    {
        $fileItems = $this->load();

        for ($i = 0; $i < count($fileItems); $i++) {
            if ($fileItems[$i]['id'] == $id) {
                unset($fileItems[$i]);
                break;
            }
        }

        unlink($this->file);

        foreach ($fileItems as $fileItem) {
            $items = [];

            foreach ($fileItem as $key => $value) {
                $items[] = urlencode($key) . ':' . urlencode($value);
            }

            file_put_contents($this->file, join(';', $items) . PHP_EOL, FILE_APPEND);
        }
    }

    private function getNewID(): int
    {
        $items = $this->load();

        $ids = [];

        if ($items !== null && count($items) > 0) {
            foreach ($items as $item) {
                $ids[] = $item['id'];
            }
        } else {
            return 0;
        }

        if (count($ids) > 0) {
            return max($ids) + 1;
        } else {
            return 0;
        }
    }
}