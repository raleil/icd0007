<?php
namespace APP\storage\file;

use APP\model\Author;
use APP\storage\AuthorStorage;

class FileAuthorStorage extends AuthorStorage
{
    private File $file;

    public function __construct(File $file)
    {
        $this->file = $file;
        $this->converter = $file->converter;
    }

    public function put(Author $author): ?int
    {
        return $this->file->save([
            'id' => $author->getID(),
            'firstName' => $author->getFirstName(),
            'lastName' => $author->getLastName(),
            'grade' => $author->getGrade()
        ]);
    }

    public function get(int $id): ?array
    {
        $authors = $this->file->load();

        foreach ($authors as $author) {
            if (isset($author['id']) && $author['id'] == $id) {
                return $author;
            }
        }

        return null;
    }

    public function getAll(): ?array
    {
        return $this->file->load();
    }

    public function delete(int $id)
    {
        $this->file->remove($id);
    }

    public function update(Author $author)
    {
        $this->delete($author->getID());
        $this->put($author);
    }
}