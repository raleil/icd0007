<?php
namespace APP\storage\file;

use APP\model\Author;
use APP\model\Book;
use APP\storage\Converter;

class FileConverter implements Converter
{
    public static function dataToBook(array $data): Book
    {
        $book = new Book();

        $book->setID($data['id']);
        $book->setTitle($data['title']);
        $book->setGrade($data['grade']);
        $book->setIsRead($data['isRead']);

        return $book;
    }

    public static function dataToAuthor(array $data): Author
    {
        if (isset($data['author'])) {
            $data = $data['author'];
        }

        $author = new Author();

        $author->setID($data['id']);
        $author->setFirstName($data['firstName']);
        $author->setLastName($data['lastName']);
        $author->setGrade($data['grade']);

        return $author;
    }

    public static function dataToBookWithAuthor(array $data): Book
    {
        $book = FileConverter::dataToBook($data['book']);

        if (isset($data['author'])) {
            $book->addAuthor(FileConverter::dataToAuthor($data['author']));
        }

        return $book;
    }
}