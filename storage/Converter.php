<?php
namespace APP\storage;

use APP\model\Author;
use APP\model\Book;

interface Converter
{
    public static function dataToBook(array $data): Book;
    public static function dataToAuthor(array $data): Author;
    public static function dataToBookWithAuthor(array $data): Book;
}