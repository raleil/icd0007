<?php
namespace APP\storage;


use APP\model\Book;

abstract class BookStorage
{
    public Converter $converter;

    public function put(Book $book): ?int
    {
        return null;
    }

    public function get(int $id): ?array
    {
        return null;
    }

    public function getAll(): ?array
    {
        return null;
    }

    public function update(Book $book) {}
    public function delete(int $id) {}
}