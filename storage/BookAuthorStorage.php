<?php
namespace APP\storage;


abstract class BookAuthorStorage
{
    public Converter $converter;

    public function put(array $data): ?int
    {
        return null;
    }

    public function getBookWithAuthors(int $id): ?array
    {
        return null;
    }

    public function getBooksWithAuthors(): ?array
    {
        return null;
    }

    public function deleteByBook(int $id) {}
}