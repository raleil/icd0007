<?php
namespace APP\model;


class Author extends Item
{
    private string $firstName;
    private string $lastName;
    private int $grade = 0;

    public static function fromDict(array $data): Author
    {
        $author = new Author();

        if (isset($data['id'])) {
            $author->setID($data['id']);
        }

        if (isset($data['firstName'])) {
            $author->setFirstName($data['firstName']);
        }

        if (isset($data['lastName'])) {
            $author->setLastName($data['lastName']);
        }

        if (isset($data['grade'])) {
            $author->setGrade($data['grade']);
        }

        return $author;
    }

    public static function validate(?Author $author): ?array
    {
        $errors = [];

        if ($author->getFirstName() === null) {
            $errors[] = "Trying to set empty title for book!";
        } else if (strlen($author->getFirstName()) < 1 || 21 < strlen($author->getFirstName())) {
            $errors[] = "Eesnime pikkus peab olema 1 kuni 21 märki.";
        }

        if ($author->getLastName() === null) {
            $errors[] = "Trying to set empty title for book!";
        } else if (strlen($author->getLastName()) < 2 || 22 < strlen($author->getLastName())) {
            $errors[] = "Perekonnanime pikkus peab olema 2 kuni 22 märki.";
        }

        if (0 > $author->getGrade() || $author->getGrade() > 5) {
            $errors[] = "Hinne peab olema 0 ja 5 vahemikus.";
        }

        return $errors;
    }

    public static function toDict(Author $author): array
    {
        return [
            'id' => $author->getID(),
            'firstName' => $author->getFirstName(),
            'lastName' => $author->getLastName(),
            'grade' => $author->getGrade()
        ];
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    public function getGrade(): int {
        return $this->grade;
    }

    public function setGrade(int $grade) {
        $this->grade = $grade;
    }

    public function getName(): string {
        return $this->getFirstName() . " " . $this->getLastName();
    }
}