<?php
namespace APP\model;

abstract class Item
{
    protected ?int $id = null;

    public function getID(): ?int {
        return $this->id;
    }

    public function setID(?int $id) {
        if ($id !== null) {
            $this->id = $id;
        }
    }
}