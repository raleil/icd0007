<?php
namespace APP\model;


class Book extends Item
{
    protected string $title;
    protected array $authors = [];
    protected int $grade = 0;
    protected bool $isRead = false;

    public static function fromDict(array $data): Book
    {
        $book = new Book();

        if (isset($data['id'])) {
            $book->setID($data['id']);
        }

        if (isset($data['title'])) {
            $book->setTitle($data['title']);
        }

        if (isset($data['grade'])) {
            $book->setGrade($data['grade']);
        }

        if (isset($data['isRead'])) {
            $book->setIsRead($data['isRead']);
        }

        return $book;
    }

    public static function validate(?Book $book): ?array
    {
        $errors = [];

        if ($book->getTitle() === null) {
            $errors[] = "Trying to set empty title for book!";
        } else if (strlen($book->getTitle()) < 3 || 23 < strlen($book->getTitle())) {
            $errors[] = "Pealkirja pikkuspeab olema 3 kuni 23 märki.";
        }

        if (0 > $book->getGrade() || $book->getGrade() > 5) {
            $errors[] = "Hinne peab olema 0 ja 5 vahemikus.";
        }

        return $errors;
    }

    public static function toDict(Book $book): array
    {
        $data = [
            'id' => $book->getID(),
            'title' => $book->getTitle(),
            'authors' => [],
            'grade' => $book->getGrade(),
            'isRead' => $book->getIsRead()
        ];

        $authors = $book->getAuthors();

        foreach ($authors as $author) {
            $data['authors'][] = Author::toDict($author);
        }

        return $data;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }

    public function addAuthor(Author $author)
    {
        if (count($this->authors) < 2) {
            $this->authors[] = $author;
        }
    }

    public function getGrade(): int
    {
        return $this->grade;
    }

    public function setGrade(int $grade)
    {
        $this->grade = $grade;
    }

    public function getIsRead(): bool
    {
        return $this->isRead;
    }

    public function setIsRead(bool $isRead)
    {
        $this->isRead = $isRead;
    }
}