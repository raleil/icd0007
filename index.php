<?php
namespace APP;

use APP\router\RouteCollection;
use APP\router\Router;

include_once("autoloader.php");

$router = new Router('cmd');



$router->addRedirect("/", "/index.html", null);

$bookRoutes = new RouteCollection('/book');

$bookRoutes->addRoute('List', 'GET', "APP\controller\BookListController@index", true);
$bookRoutes->addRoute('Form', 'GET', "APP\controller\BookFormController@index");
$bookRoutes->addRoute('Form', 'POST', "APP\controller\BookFormController@post");
$bookRoutes->set404("APP\controller\ErrorController@pageNotFoundHtml");

$router->addRouteCollection($bookRoutes);

$authorRoutes = new RouteCollection('/author');

$authorRoutes->addRoute('List', 'GET', "APP\controller\AuthorListController@index", true);
$authorRoutes->addRoute('Form', 'GET', "APP\controller\AuthorFormController@index");
$authorRoutes->addRoute('Form', 'POST', "APP\controller\AuthorFormController@post");
$authorRoutes->set404("APP\controller\ErrorController@pageNotFoundHtml");

$router->addRouteCollection($authorRoutes);



$router->addRedirect('/api', '/api/book', 'findAll');

$bookAPIRoutes = new RouteCollection('/api/book');

$bookAPIRoutes->addRoute('findAll', 'GET', "APP\controller\BookAPIController@findAll", true);
$bookAPIRoutes->addRoute('findById', 'GET', "APP\controller\BookAPIController@findById");
$bookAPIRoutes->addRoute('add', 'POST', "APP\controller\BookAPIController@add");
$bookAPIRoutes->addRoute('edit', 'POST', "APP\controller\BookAPIController@edit");
$bookAPIRoutes->addRoute('delete', 'POST', "APP\controller\BookAPIController@delete");
$bookAPIRoutes->set404("APP\controller\ErrorController@pageNotFoundJson");

$router->addRouteCollection($bookAPIRoutes);

$authorAPIRoutes = new RouteCollection('/api/author');

$authorAPIRoutes->addRoute('findAll', 'GET', "APP\controller\AuthorAPIController@findAll", true);
$authorAPIRoutes->addRoute('findById', 'GET', "APP\controller\AuthorAPIController@findById");
$authorAPIRoutes->addRoute('add', 'POST', "APP\controller\AuthorAPIController@add");
$authorAPIRoutes->addRoute('edit', 'POST', "APP\controller\AuthorAPIController@edit");
$authorAPIRoutes->addRoute('delete', 'POST', "APP\controller\AuthorAPIController@delete");
$authorAPIRoutes->set404("APP\controller\ErrorController@pageNotFoundJson");

$router->addRouteCollection($authorAPIRoutes);

echo $router->navigate();