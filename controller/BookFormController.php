<?php
namespace APP\controller;


use APP\model\Author;
use APP\model\Book;

class BookFormController extends Controller
{
    public function index(array $args): string
    {
        $data = [];

        if (isset($args['id'])) {
            $data['book'] = Book::toDict($this->bookAuthorRepository->getBookWithAuthors($args['id']));
        } else if (isset($args['book'])) {
            $data['book'] = $args['book'];
        }

        $authors = $this->authorRepository->loadAuthors();

        if (count($authors) > 0) {
            foreach ($authors as $author) {
                $data['authors'][] = Author::toDict($author);
            }
        }

        if (isset($args['errors'])) {
            $data['errors'] = $args['errors'];
        }

        if (isset($args['edit']) && $args['edit']) {
            $data['edit'] = true;
        }

        return $this->twig->render('@book/form.twig', $data);
    }

    public function post(array $args)
    {
        $book = Book::fromDict($args);
        $authorIDs = null;

        if (isset($args['author1']) && strlen($args['author1']) > 0 && is_numeric($args['author1'])) {
            $authorIDs[] = $args['author1'];
        }

        if (isset($args['author2']) && strlen($args['author2']) > 0 && is_numeric($args['author2'])) {
            $authorIDs[] = $args['author2'];
        }

        if (isset($args['submitButton'])) {
            $errors = Book::validate($book);

            if (count($errors) > 0) {
                $bookDict = Book::toDict($book);

                if ($authorIDs !== null && count($authorIDs) > 0) {
                    foreach ($authorIDs as $authorID) {
                        $bookDict['authors'][] = [
                            "id" => $authorID
                        ];
                    }
                }

                return $this->index([
                    'edit' => isset($args['edit']),
                    'book' => $bookDict,
                    'errors' => $errors
                ]);
            }

            if (isset($args['edit'])) {
                $this->bookRepository->updateBook($book);
                $this->bookAuthorRepository->updateBookAuthors($book, $authorIDs);

                header('Location: /book?cmd=List&success=' . urlencode("Muudetud!"));
            } else {
                $this->bookRepository->addBook($book);
                $this->bookAuthorRepository->addBookAuthors($book, $authorIDs);

                header('Location: /book?cmd=List&success=' . urlencode("Lisatud!"));
            }
        } else if (isset($args['deleteButton'])) {
            $this->bookRepository->removeBook($book->getID());

            header('Location: /book?cmd=List&success=' . urlencode("Kustutatud!"));
        }

        return null;
    }
}