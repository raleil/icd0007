<?php


namespace APP\controller;


use APP\model\Book;

class BookAPIController extends Controller
{
    public function findAll(array $args)
    {
        $books = $this->bookAuthorRepository->getBooksWithAuthors();

        $data = [];

        foreach ($books as $book) {
            $data[] = Book::toDict($book);
        }

       return $this->json($data);
    }

    public function findById(array $args)
    {
        $book = $this->bookAuthorRepository->getBookWithAuthors($args['id']);

        if ($book === null) {
            http_response_code(404);
            return $this->json(["errors" => ["Book with ID ${args['id']} doesn't exist"]]);
        }

        return $this->json(Book::toDict($book));
    }

    public function add(array $args)
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json, true);

        $book = Book::fromDict($data);

        $errors = Book::validate($book);

        if (count($errors) > 0) {
            http_response_code(400);
            return $this->json(["errors" => $errors]);
        }

        $this->bookRepository->addBook($book);
        $this->bookAuthorRepository->addBookAuthors($book, $data["authors"]);

        return $this->json($data);
    }

    public function edit(array $args)
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json, true);

        $book = Book::fromDict($data);

        $errors = Book::validate($book);

        if (count($errors) > 0) {
            http_response_code(400);
            return $this->json(["errors" => $errors]);
        }

        $this->bookRepository->updateBook($book);
        $this->bookAuthorRepository->updateBookAuthors($book, $data["authors"]);

        return $this->json($data);
    }

    public function delete(array $args)
    {
        $bookId = null;

        if (isset($_GET['id'])) {
            $bookId = intval($_GET['id']);
        }

        if ($bookId === null) {
            http_response_code(400);
            return $this->json(["errors" => "Book with ID {$bookId} doesnt exist!"]);
        }

        $this->bookRepository->removeBook($bookId);
        http_response_code(204);

        return $this->json(["success" => "Book with ID {$bookId} has been removed!"]);
    }
}