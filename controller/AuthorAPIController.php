<?php


namespace APP\controller;


use APP\model\Author;
use APP\model\Book;

class AuthorAPIController extends Controller
{
    public function findAll(array $args)
    {
        $authors = $this->authorRepository->loadAuthors();

        $data = [];

        foreach ($authors as $author) {
            $data[] = Author::toDict($author);
        }

       return $this->json($data);
    }

    public function findById(array $args)
    {
        $author = $this->authorRepository->loadAuthor($args['id']);

        if ($author === null) {
            http_response_code(404);
            return $this->json(["errors" => ["Author with ID ${args['id']} doesn't exist"]]);
        }

        return $this->json(Author::toDict($author));
    }

    public function add(array $args)
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json, true);

        $author = Author::fromDict($data);

        $errors = Author::validate($author);

        if (count($errors) > 0) {
            http_response_code(400);
            return $this->json(["errors" => $errors]);
        }

        $this->authorRepository->addAuthor($author);

        return $this->json($data);
    }

    public function edit(array $args)
    {
        $json = file_get_contents("php://input");
        $data = json_decode($json, true);

        $author = Author::fromDict($data);

        $errors = Author::validate($author);

        if (count($errors) > 0) {
            http_response_code(400);
            return $this->json(["errors" => $errors]);
        }

        $this->authorRepository->updateAuthor($author);

        return $this->json($data);
    }

    public function delete(array $args)
    {
        $authorId = null;

        if (isset($_GET['id'])) {
            $authorId = intval($_GET['id']);
        }

        if ($authorId === null) {
            http_response_code(400);
            return $this->json(["errors" => "Author with ID {$authorId} doesnt exist!"]);
        }

        $this->authorRepository->removeAuthor($authorId);
        http_response_code(204);

        return $this->json(["success" => "Author with ID {$authorId} has been removed!"]);
    }
}