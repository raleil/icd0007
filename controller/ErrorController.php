<?php


namespace APP\controller;


class ErrorController extends Controller
{
    public function pageNotFoundHtml(string $page): string
    {
        return $this->twig->render('@error/pageNotFound.twig', ["page" => $page]);
    }

    public function pageNotFoundJson(string $page): string
    {
        http_response_code(400);
//        return $this->json($_SERVER);
        return $this->json(["error" => "Unknown command: ${page}"]);
    }
}