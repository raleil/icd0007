<?php
namespace APP\controller;


use APP\model\Author;

class AuthorListController extends Controller
{
    public function index(array $args): string
    {
        $authors = $this->authorRepository->loadAuthors();

        $data = [];

        foreach ($authors as $author) {
            $data['authors'][] = Author::toDict($author);
        }

        if (isset($args['success'])) {
            $data['success'] = $args['success'];
        }

        return $this->twig->render('@author/list.twig', $data);
    }
}