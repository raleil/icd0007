<?php
namespace APP\controller;

use APP\model\Book;

class BookListController extends Controller
{
    public function index(array $args): string
    {
        $books = $this->bookAuthorRepository->getBooksWithAuthors();

        $data = [];

        foreach ($books as $book) {
            $data['books'][] = Book::toDict($book);
        }

        if (isset($args['success'])) {
            $data['success'] = $args['success'];
        }

        return $this->twig->render('@book/list.twig', $data);
    }
}




