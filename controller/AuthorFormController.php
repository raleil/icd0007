<?php
namespace APP\controller;


use APP\model\Author;

class AuthorFormController extends Controller
{
    public function index(array $args): String
    {
        $data = [];

        if (isset($args['id'])) {
            $data['author'] = Author::toDict($this->authorRepository->loadAuthor($args['id']));
        } else if (isset($args['author'])) {
            $data['author'] = $args['author'];
        }

        if (isset($args['errors'])) {
            $data['errors'] = $args['errors'];
        }

        if (isset($args['edit']) && $args['edit']) {
            $data['edit'] = true;
        }

        return $this->twig->render('@author/form.twig', $data);
    }

    public function post(array $args)
    {
        $author = Author::fromDict($args);

        if (isset($args['submitButton'])) {
            $errors = Author::validate($author);

            if (count($errors) > 0) {
                return $this->index([
                    'edit' => isset($args['edit']),
                    'author' => Author::toDict($author),
                    'errors' => $errors
                ]);
            }

            if (isset($args['edit'])) {
                $this->authorRepository->updateAuthor($author);

                header('Location: /author?cmd=List&success=' . urlencode("Muudetud!"));
            } else {
                $this->authorRepository->addAuthor($author);

                header('Location: /author?cmd=List&success=' . urlencode("Lisatud!"));
            }
        } else if (isset($args['deleteButton'])) {
            $this->authorRepository->removeAuthor($author->getID());

            header('Location: /author?cmd=List&success=' . urlencode("Kustutatud!"));
        }

        return null;
    }
}