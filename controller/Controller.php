<?php
namespace APP\controller;


use APP\repository\AuthorRepository;
use APP\repository\BookAuthorRepository;
use APP\repository\BookRepository;
use APP\storage\file\File;
use APP\storage\file\FileAuthorStorage;
use APP\storage\file\FileBookAuthorStorage;
use APP\storage\file\FileBookStorage;
use APP\storage\mysql\Database;
use APP\storage\mysql\MysqlAuthorStorage;
use APP\storage\mysql\MysqlBookAuthorStorage;
use APP\storage\mysql\MysqlBookStorage;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class Controller
{
    const USE_MYSQL = true;

    public Environment $twig;

    public BookRepository $bookRepository;
    public AuthorRepository $authorRepository;
    public BookAuthorRepository $bookAuthorRepository;

    public function __construct()
    {
        $this->initTwig();
        $this->initStorage();
    }

    private function initTwig()
    {
        $loader = new FilesystemLoader();

        $loader->addPath("views/author", 'author');
        $loader->addPath("views/book", 'book');
        $loader->addPath("views/error", 'error');
        $loader->addPath("views/templates", 'templates');

        $this->twig = new Environment($loader);
    }

    private function initStorage()
    {
        if (Controller::USE_MYSQL) {
            $database = Database::getInstance();

            $bookStorage = new MysqlBookStorage($database);
            $authorStorage = new MysqlAuthorStorage($database);
            $bookAuthorStorage = new MysqlBookAuthorStorage($database);
        } else {
            $bookStorage = new FileBookStorage(new File(realpath("data") . "\\books.txt"));
            $authorStorage = new FileAuthorStorage(new File(realpath("data") . "\\authors.txt"));
            $bookAuthorStorage = new FileBookAuthorStorage($bookStorage, $authorStorage, new File(realpath("data") . "\\booksauthors.txt"));
        }

        $this->bookRepository = new BookRepository($bookStorage);
        $this->authorRepository = new AuthorRepository($authorStorage);
        $this->bookAuthorRepository = new BookAuthorRepository($bookAuthorStorage);
    }

    public function json($object)
    {
        header("Content-Type: application/json");
        return json_encode($object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }
}