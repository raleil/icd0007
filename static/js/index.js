import { router, navigateTo } from "./router.js";

window.addEventListener("popstate", router);

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener('click', e => {
        if (e.target.matches("[data-link]")) {
            e.preventDefault();

            if (location.href !== e.target.href) {
                navigateTo(e.target.href);
            } else {
                location.reload();
            }
        }
    });

    router();
})