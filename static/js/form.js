import {Element} from "./elements.js";

export function checkboxInput(id, isChecked) {
    const radio = Element.get("input");

    radio.id = id;
    radio.checked = isChecked;
    radio.type = "checkbox";

    return radio;
}

export function formCheckboxGroup(labelValue, radioId, isChecked) {
    const label = Element.withText("label", labelValue);
    label.for = radioId;

    const input = checkboxInput(radioId, isChecked);

    return Element.withChildren("tr",
        Element.withChildren("td", label),
        Element.withChildren("td", input)
    );
}

export function radioInput(radioId, value, text, isChecked) {
    const radio = Element.get("input");

    radio.checked = isChecked;
    radio.type = "radio";
    radio.name = radioId;
    radio.id = radioId;

    if (value) {
        radio.value = value;
    }

    const label = new Element("label");

    label.addChild(radio)
    label.addTextChild(text + " ");

    return label.getElem;
}

export function formRadioGroup(labelName, radioId, selectedValue, values) {
    const label = Element.withText("label", labelName);

    const radioGroup = new Element('td');

    let toggle = false;

    values.forEach(value => {
        if (!selectedValue && !toggle) {
            const radio = radioInput(radioId, value["value"], value["text"], true);
            radioGroup.addChild(radio);
            toggle = true;
        } else {
            const radio = radioInput(radioId, value["value"], value["text"], value["value"] === selectedValue);
            radioGroup.addChild(radio);
        }
    });

    return Element.withChildren("tr",
        Element.withChildren("td", label),
        radioGroup.getElem
    );
}

export function textInput(className, id, value) {
    const input = Element.get("input");

    input.className = className;
    input.id = id;
    input.name = id;
    input.type = "text";

    if (value) {
        input.value = value;
    }

    return input;
}

export function formTextGroup(labelValue, inputId, inputValue) {
    const label = Element.withText("label", labelValue);
    label.for = inputId;
    const input = textInput("", inputId, inputValue);

    return Element.withChildren("tr",
        Element.withChildren("td", label),
        Element.withChildren("td", input)
    );
}

export function hiddenInput(id, value) {
    const input = Element.get("input");

    input.id = id;
    input.type = "hidden";

    if (value) {
        input.value = value;
    }

    return input;
}

export function optionInput(value, text, isSelected) {
    const option = Element.withText("option", text);
    option.selected = isSelected;

    if (value) {
        option.value = value;
    }

    return option;
}

export function formOptionGroup(labelValue, selectId, selectedValue, values) {
    const label = Element.withText("label", labelValue);
    label.for = selectId;

    let select = new Element("select");

    select.addChild(optionInput("", "", false));

    values.forEach(value => {
        const option = optionInput(value["value"], value["text"], value["value"] === selectedValue);

        select.addChild(option);
    });

    select = select.getElem;
    select.id = selectId;

    return Element.withChildren("tr",
        Element.withChildren("td", label),
        Element.withChildren("td", select)
    );
}