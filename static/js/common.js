import {formOptionGroup, formRadioGroup} from "./form.js";

export async function createAuthorFormGroup(labelValue, selectId, selectedAuthor) {
    const authors = await fetch("/api/author?cmd=findAll")
        .then(response => response.json());

    const values = authors.map(author => {
        return {
            "value": author.id,
            "text": author.firstName + " " + author.lastName
        }
    })

    return formOptionGroup(labelValue, selectId, selectedAuthor, values);
}

export function createGradeFormGroup(labelValue, radioId, selectedGrade) {
    const options = [];

    for (let i = 0; i <= 5; i++) {
        options.push({
            value: i,
            text: i
        });
    }

    return formRadioGroup(labelValue, radioId, selectedGrade, options);
}