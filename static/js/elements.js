export function elem(tagName, text, children) {
    const element = document.createElement(tagName);

    if (text) {
        const textNode = document.createTextNode(text);
        element.appendChild(textNode);
    }

    console.log(children)

    if (children !== null) {
        children.forEach(node => {
            element.appendChild(node);
        });
    }

    return element;
}

export class Element {
    constructor(tagName) {
        this.element = document.createElement(tagName);
    }

    get getElem() {
        return this.element;
    }

    addTextChild(text) {
        this.addChild(document.createTextNode(text));
    }

    addChild(child) {
        this.element.appendChild(child);
    }

    static get(tagName) {
        const element = new Element(tagName);

        return element.getElem;
    }

    static withText(tagName, text) {
        const element = new Element(tagName);

        element.addTextChild(text);

        return element.getElem;
    }

    static withChildren(tagName, ...children) {
        const element = new Element(tagName);

        for (let child of children) {
            element.addChild(child);
        }

        return element.getElem;
    }
}


export function errorBlock(errors) {
    const errorBlock = new Element("div");

    for (let err of errors) {
        const listItem = Element.withText("p", err);
        errorBlock.addChild(listItem);
    }

    return errorBlock.getElem;
}

export function displayErrors(errorList) {
    clearExistingErrorBlock();
    const errors = errorBlock(errorList);
    errors.id = "error-block";

    const message = document.getElementById("message");
    message.className = "is-danger"
    message.appendChild(errors)
}

export function displaySuccess(msg) {
    clearExistingErrorBlock();
    const success = Element.withText("div", msg);
    success.id = "message-block"

    const message = document.getElementById("message");
    message.className = "is-success"
    message.appendChild(success)
}

function clearExistingErrorBlock() {
    const errorBlock = document.getElementById("message");
    errorBlock.innerHTML = "";
}