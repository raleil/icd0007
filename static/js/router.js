import books from "./views/books.js";
import book from "./views/book.js";
import authors from "./views/authors.js";
import author from "./views/author.js";

export const navigateTo = (path) => {
    history.pushState(null, null, path);
    router();
}

const removeAllChildNodes = (element) => {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

export const router = async () => {
    const routes = [
        { path: "/index.html/book", renderView: book },
        { path: "/index.html/book/list", renderView: books},
        { path: "/index.html/author", renderView: author},
        { path: "/index.html/author/list", renderView: authors}
    ];

    const defaultRoute = routes[1];
    const currentPath = location.pathname;
    const params = new URLSearchParams(location.search);

    const routeMatch = routes.find(route => route.path === currentPath);

    const app = document.getElementById("app");
    const message = document.getElementById("message");
    message.innerHTML = "";
    removeAllChildNodes(app);

    if (routeMatch) {
        const view = await routeMatch.renderView(params)
        app.appendChild(view);
    } else {
        const view = await defaultRoute.renderView(params);
        app.appendChild(view);
    }
}