import { navigateTo } from "../router.js";
import { Element, displayErrors } from "../elements.js";
import {formCheckboxGroup, formTextGroup, hiddenInput} from "../form.js";
import {createAuthorFormGroup, createGradeFormGroup} from "../common.js";

const book = async (params) => {
    document.title = "Raamat";
    const page = new Element("div");

    const bookId = params.get("id");
    let book = null;
    if (bookId) {
        book = await fetch(`/api/book?cmd=findById&id=${encodeURIComponent(bookId)}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.status);
                }

                return response;
            })
            .then(response => response.json())
            .catch(error => displayErrors(["Sellist raamatut ei leitud!"]));
    }

    const title = Element.withText("h1", book ? "Muuda raamatut" : "Lisa uus raamat");
    title.className = "text-center";
    page.addChild(title);

    const bookEditForm = await createBookEditForm(book);
    page.addChild(bookEditForm);

    return page.getElem;
}

async function createBookEditForm(book) {
    let titleGroup;
    let author1Group;
    let author2Group;
    let gradeGroup;
    let isReadGroup;

    const query = book ? '/api/book?cmd=edit' : '/api/book?cmd=add';

    if (book) {
        titleGroup = formTextGroup("Pealkiri: ", "title", book.title);
        author1Group = await createAuthorFormGroup("Autor 1: ", "autor1", book.authors[0] ? book.authors[0].id : null);
        author2Group = await createAuthorFormGroup("Autor 2: ", "autor2", book.authors[1] ? book.authors[1].id : null);
        gradeGroup = createGradeFormGroup("Hinne: ", "grade", book.grade);
        isReadGroup = formCheckboxGroup("Loetud: ", "isRead",  book.isRead);
    } else {
        titleGroup = formTextGroup("Pealkiri: ", "title");
        author1Group = await createAuthorFormGroup("Autor 1: ", "autor1");
        author2Group = await createAuthorFormGroup("Autor 2: ", "autor2");
        gradeGroup = createGradeFormGroup("Hinne: ", "grade");
        isReadGroup = formCheckboxGroup("Loetud: ", "isRead");
    }

    const submitButton = Element.get("input");
    submitButton.className = "is-info";
    submitButton.type = "submit";
    submitButton.name = "submitButton";
    submitButton.value = book ? "Muuda" : "Salvesta";
    submitButton.addEventListener("click", e => {
        const title = document.getElementById("title").value;
        const author1 = document.getElementById("autor1").value;
        const author2 = document.getElementById("autor2").value;
        const grade = document.querySelector('input[name="grade"]:checked').value;
        const isRead = document.getElementById("isRead").checked;

        const newBook = {
            title: title,
            authors: [parseInt(author1), parseInt(author2)],
            grade: parseInt(grade),
            isRead: isRead
        };

        if (book) {
            newBook['id'] = book.id
        }

        fetch(query, {
            method: "POST",
            body: JSON.stringify(newBook)
        })
            .then(async response => {
                if (response.ok) {
                    return response.json();
                }
                return response.json().then(errorBody => {
                    const error = new Error(response.statusText);
                    error.errorBody = errorBody;
                    throw error;
                });
            })
            .then(data => {
                navigateTo("/index.html/book/list?success=true", );
            })
            .catch(error => displayErrors(error.errorBody.errors));

    });

    const buttons = new Element('tr');

    if (book) {
        const tableDelete = Element.withChildren('td', deleteBookButton(book));
        tableDelete.className = "button-left";
        buttons.addChild(tableDelete);
    } else {
        buttons.addChild(Element.get('td'));
    }

    const tableSubmit = Element.withChildren('td', submitButton);
    tableSubmit.className = "button-right";
    buttons.addChild(tableSubmit);

    const table = Element.withChildren('table',
        Element.withChildren('tbody',
            titleGroup,
            author1Group,
            author2Group,
            gradeGroup,
            isReadGroup,
            buttons.getElem
        ));

    table.className = "form-table";

    const form = Element.withChildren("form", table);

    form.onsubmit = function () {
        return false;
    };

    return form;
}

function deleteBookButton(book) {
    const button = Element.get("input");
    button.className = "is-danger";
    button.type = "submit";
    button.name = "deleteButton";
    button.value = "Kustuta";

    button.addEventListener("click", e => {
        fetch(`/api/book?cmd=delete&id=${book.id}`, { method: "POST" })
            .then(data => {
                navigateTo("/index.html/book/list?success=true", );
            });
    });


    return button;
}

export default book;