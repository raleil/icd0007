import {displaySuccess, Element} from "../elements.js";

const books = async (params) => {
    document.title = "Raamatud";

    const books = await fetch("/api/book?cmd=findAll")
        .then(response => response.json());

    const page = document.createElement("div");
    const title = document.createElement("h1");
    title.textContent = "Raamatud";
    title.className = "text-center";

    page.appendChild(title);

    const tbody = Element.get("tbody");

    books.forEach(book => {
        const row = Element.withChildren("tr",
            Element.withChildren("td", linkToBook(book)),
            Element.withText("td", book.authors.map(author => author.firstName + " " + author.lastName).join(", ")),
            Element.withText("td", book.grade)
        );

        tbody.appendChild(row);
    });

    const table = Element.withChildren("table",
        Element.withChildren("thead",
            Element.withChildren("tr",
                Element.withText("th", "Pealkiri"),
                Element.withText("th", "Autorid"),
                Element.withText("th", "Hinne")
            )
        ),
        tbody
    )
    table.className = "list-table";

    if (params.get('success') && params.get('success') === "true") {
        displaySuccess("Salvestamine õnnestus");
    }

    page.appendChild(table);
    return page;
}

function linkToBook(book) {
    const link = Element.withText("a", book.title);

    link.href = `/index.html/book?id=${encodeURIComponent(book.id)}`;
    link.setAttribute("data-link", "");

    return link;
}

export default books;