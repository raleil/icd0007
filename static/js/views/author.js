import { navigateTo } from "../router.js";
import { Element, displayErrors } from "../elements.js";
import {formCheckboxGroup, formTextGroup, hiddenInput} from "../form.js";
import {createAuthorFormGroup, createGradeFormGroup} from "../common.js";

const author = async (params) => {
    document.title = "Autor";
    const page = new Element("div");



    const authorId = params.get("id");
    let author = null;
    if (authorId) {
        author = await fetch(`/api/author?cmd=findById&id=${encodeURIComponent(authorId)}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error(response.status);
                }

                return response;
            })
            .then(response => response.json())
            .catch(error => displayErrors(["Sellist autorit ei leitud!"]));
    }

    const title = Element.withText("h1", author ? "Muuda autorit" : "Lisa uus autor");
    title.className = "text-center";
    page.addChild(title);

    const authorEditForm = await createAuthorEditForm(author);
    page.addChild(authorEditForm);

    return page.getElem;
}

async function createAuthorEditForm(author) {
    let firstNameGroup;
    let lastNameGroup;
    let gradeGroup;

    const query = author ? '/api/author?cmd=edit' : '/api/author?cmd=add';

    if (author) {
        firstNameGroup = formTextGroup("Eesnimi: ", "firstName", author.firstName);
        lastNameGroup = formTextGroup("Perekonnanimi: ", "lastName", author.lastName);
        gradeGroup = createGradeFormGroup("Hinne: ", "grade", author.grade);
    } else {
        firstNameGroup = formTextGroup("Eesnimi: ", "firstName");
        lastNameGroup = formTextGroup("Perekonnanimi: ", "lastName");
        gradeGroup = createGradeFormGroup("Hinne: ", "grade");
    }

    const submitButton = Element.get("input");
    submitButton.className = "is-info";
    submitButton.type = "submit";
    submitButton.name = "submitButton";
    submitButton.value = author ? "Muuda" : "Salvesta";
    submitButton.addEventListener("click", e => {
        const firstName = document.getElementById("firstName").value;
        const lastName = document.getElementById("lastName").value;
        const grade = document.querySelector('input[name="grade"]:checked').value;

        const newAuthor = {
            firstName: firstName,
            lastName: lastName,
            grade: parseInt(grade)
        };

        if (author) {
            newAuthor['id'] = author.id
        }

        fetch(query, {
            method: "POST",
            body: JSON.stringify(newAuthor)
        })
            .then(async response => {
                if (response.ok) {
                    return response.json();
                }
                return response.json().then(errorBody => {
                    const error = new Error(response.statusText);
                    error.errorBody = errorBody;
                    throw error;
                });
            })
            .then(data => {
                navigateTo("/index.html/author/list?success=true", );
            })
            .catch(error => displayErrors(error.errorBody.errors));

    });

    const buttons = new Element('tr');

    if (author) {
        const tableDelete = Element.withChildren('td', deleteAuthorButton(author));
        tableDelete.className = "button-left";
        buttons.addChild(tableDelete);
    } else {
        buttons.addChild(Element.get('td'));
    }

    const tableSubmit = Element.withChildren('td', submitButton);
    tableSubmit.className = "button-right";
    buttons.addChild(tableSubmit);

    const table = Element.withChildren('table',
        Element.withChildren('tbody',
            firstNameGroup,
            lastNameGroup,
            gradeGroup,
            buttons.getElem
        ));

    table.className = "form-table";

    const form = Element.withChildren("form", table);

    form.onsubmit = function () {
        return false;
    };

    return form;
}

function deleteAuthorButton(author) {
    const button = Element.get("input");
    button.className = "is-danger";
    button.type = "submit";
    button.name = "deleteButton";
    button.value = "Kustuta";

    button.addEventListener("click", e => {
        fetch(`/api/author?cmd=delete&id=${author.id}`, { method: "POST" })
            .then(data => {
                navigateTo("/index.html/author/list?success=true", );
            });
    });


    return button;
}

export default author;