import {displaySuccess, Element} from "../elements.js";

const authors = async (params) => {
    document.title = "Autorid";

    const authors = await fetch("/api/author?cmd=findAll")
        .then(response => response.json());

    const page = document.createElement("div");
    const title = document.createElement("h1");
    title.textContent = "Autorid";
    title.className = "text-center";

    page.appendChild(title);

    const tbody = Element.get("tbody");

    authors.forEach(author => {
        const row = Element.withChildren("tr",
            Element.withChildren("td", linkToAuthor(author)),
            Element.withText("td", author.lastName),
            Element.withText("td", author.grade)
        );

        tbody.appendChild(row);
    });

    const table = Element.withChildren("table",
        Element.withChildren("thead",
            Element.withChildren("tr",
                Element.withText("th", "Nimi"),
                Element.withText("th", "Perekonnanimi"),
                Element.withText("th", "Hinne")
            )
        ),
        tbody
    )
    table.className = "list-table";

    if (params.get('success') && params.get('success') === "true") {
        displaySuccess("Salvestamine õnnestus");
    }

    page.appendChild(table);
    return page;
}

function linkToAuthor(author) {
    const link = Element.withText("a", author.firstName);

    link.href = `/index.html/author?id=${encodeURIComponent(author.id)}`;
    link.setAttribute("data-link", "");

    return link;
}

export default authors;