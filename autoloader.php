<?php

namespace APP;


spl_autoload_register(function ($Class) {
    // Cut Root-Namespace
    $Class = str_replace(__NAMESPACE__.'\\', '', $Class);
    // Correct DIRECTORY_SEPARATOR
    $Class = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.$Class.'.php');
    // Get file real path
    if(false === ($Class = realpath($Class))) {
        // File not found
        return false;
    } else {
        require_once($Class);
        return true;
    }
});

require_once 'vendor/autoload.php';
