<?php
namespace APP\repository;

use APP\model\Book;
use APP\storage\BookStorage;

class BookRepository
{
    private BookStorage $storage;

    public function __construct(BookStorage $storage)
    {
        $this->storage = $storage;
    }

    public function addBook(Book $book): ?int
    {
        $id = $this->storage->put($book);

        $book->setID($id);

        return $id;
    }

    public function updateBook(Book $book)
    {
        $this->storage->update($book);
    }

    public function removeBook(int $id)
    {
        $this->storage->delete($id);
    }
}