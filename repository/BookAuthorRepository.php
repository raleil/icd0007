<?php
namespace APP\repository;

use APP\model\Book;
use APP\model\Item;
use APP\storage\BookAuthorStorage;

class BookAuthorRepository
{
    private BookAuthorStorage $storage;

    public function __construct(BookAuthorStorage $storage)
    {
        $this->storage = $storage;
    }

    public function getBookWithAuthors(int $id): ?Book
    {
        $bookWithAuthors = $this->storage->getBookWithAuthors($id);

        $book = null;

        if ($bookWithAuthors !== null && count($bookWithAuthors) > 0) {
            foreach ($bookWithAuthors as $item) {
                if ($book !== null) {
                    $author = $this->storage->converter::dataToAuthor($item);

                    if ($author !== null) {
                        $book->addAuthor($author);
                    }
                } else {
                    $book = $this->storage->converter::dataToBookWithAuthor($item);
                }
            }
        }

        return $book;
    }

    public function getBooksWithAuthors(): ?array
    {
        $booksWithAuthors = $this->storage->getBooksWithAuthors();
        $books = [];

        if ($booksWithAuthors !== null && count($booksWithAuthors) > 0) {
            foreach ($booksWithAuthors as $item) {
                if ($books !== null && count($books) > 0) {
                    $foundBook = null;

                    foreach ($books as $book) {
                        if ($book->getID() == $item['bookID']) {
                            $foundBook = $book;
                            break;
                        }
                    }

                    if ($foundBook !== null) {
                        $author = $this->storage->converter::dataToAuthor($item);

                        if ($author !== null) {
                            $foundBook->addAuthor($author);
                        }
                    } else {
                        $books[] = $this->storage->converter::dataToBookWithAuthor($item);
                    }
                } else {
                    $books[] = $this->storage->converter::dataToBookWithAuthor($item);
                }
            }
        }

        usort($books, function (Item $a, Item $b) {
            if ($a->getID() == $b->getID()) {
                return 0;
            }

            return ($a->getID() < $b->getID()) ? -1 : 1;
        });

        return $books;
    }

    public function addBookAuthors(Book $book, ?array $authorIDs)
    {
        if ($authorIDs !== null && count($authorIDs) > 0) {
            foreach ($authorIDs as $authorID) {
                if (is_int($authorID)) {
                    $this->linkBookWithAuthor($book->getID(), $authorID);
                }
            }
        } else {
            foreach ($book->getAuthors() as $author) {
                $this->linkBookWithAuthor($book->getID(), $author->getID());
            }
        }
    }

    public function updateBookAuthors(Book $book, ?array $authorIDs)
    {
        $this->storage->deleteByBook($book->getID());
        $this->addBookAuthors($book, $authorIDs);
    }

    private function linkBookWithAuthor(int $bookID, int $authorID)
    {
        $this->storage->put([
            'bookID' => $bookID,
            'authorID' => $authorID
        ]);
    }
}