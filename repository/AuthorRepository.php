<?php
namespace APP\repository;

use APP\model\Author;
use APP\model\Item;
use APP\storage\AuthorStorage;

class AuthorRepository
{
    private AuthorStorage $storage;

    public function __construct(AuthorStorage $storage)
    {
        $this->storage = $storage;
    }

    public function addAuthor(Author $author)
    {
        $id = $this->storage->put($author);

        $author->setID($id);

        return $id;
    }

    public function loadAuthor(int $id): ?Author {
        $rawAuthor = $this->storage->get($id);

        if ($rawAuthor === null) {
            return null;
        }

        return $this->storage->converter::dataToAuthor($rawAuthor);
    }

    public function loadAuthors(): array
    {
        $authors = $this->storage->getAll();

        $authorList = [];

        if ($authors !== null && count($authors)) {
            foreach ($authors as $author) {
                $authorList[] = $this->storage->converter::dataToAuthor($author);
            }

            usort($authorList, function (Item $a, Item $b) {
                if ($a->getID() == $b->getID()) {
                    return 0;
                }

                return ($a->getID() < $b->getID()) ? -1 : 1;
            });
        }

        return $authorList;
    }

    public function updateAuthor(Author $author)
    {
        $this->storage->update($author);
    }

    public function removeAuthor(int $id)
    {
        $this->storage->delete($id);
    }
}