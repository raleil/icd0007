<?php


namespace APP\router;


class Route
{
    private string $page;
    private string $reqMethod;
    private string $controller;
    private bool $default;

    public function __construct(string $page, string $reqMethod, string $controller, bool $default = false)
    {
        $this->page = $page;
        $this->reqMethod = $reqMethod;
        $this->controller = $controller;
        $this->default = $default;
    }

    public function getPage(): string
    {
        return $this->page;
    }

    public function getReqMethod(): string
    {
        return $this->reqMethod;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function isDefault(): bool
    {
        return $this->default;
    }
}