<?php


namespace APP\router;


class RouteCollection
{
    private string $path;
    private string $notFoundRoute;
    private array $routes;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function set404(string $controller)
    {
        $this->notFoundRoute = $controller;
    }

    public function get404(): string
    {
        return $this->notFoundRoute;
    }

    public function addRoute(string $page, string $method, string $controller, bool $default = false): RouteCollection
    {
        $this->routes[] = new Route($page, $method, $controller, $default);

        return $this;
    }

    public function getRoutes(): array
    {
        return $this->routes;
    }
}