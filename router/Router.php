<?php
namespace APP\router;


class Router
{
    private array $routeCollections = [];
    private array $redirects = [];
    private string $navArg;

    public function __construct(string $navArg)
    {
        $this->navArg = $navArg;
    }

    public function addRouteCollection(RouteCollection $routeCollection)
    {
        $this->routeCollections[] = $routeCollection;
    }

    public function addRedirect(string $path, string $newPath, ?string $page)
    {
        $this->redirects[] = [
            'path' => $path,
            'newPath' => $newPath,
            'page' => $page
        ];
    }

    private function tryRedirect()
    {
        $reqPath = $this->getReqPath();

        foreach ($this->redirects as $redirect) {
            if (strcmp($redirect['path'], $reqPath) === 0) {
                if ($redirect['page'] === null) {
                    header("Location: {$redirect['newPath']}", true, 303);
                } else {
                    header("Location: {$redirect['newPath']}?{$this->navArg}={$redirect['page']}", true, 303);
                }

                exit();
            }
        }
    }

    private function getReqPath(): string
    {
        if (isset($_SERVER['PATH_INFO'])) {
            $reqPath = $_SERVER['PATH_INFO'];
        } else {
            $reqPath = $_SERVER['REQUEST_URI'];
        }

        if (strlen($reqPath) !== 1 && substr($reqPath, -1) === '/') {
            $reqPath = substr($reqPath, 0, -1);
        }

        return $reqPath;
    }

    public function navigate(): string
    {
        $this->tryRedirect();

        $routeCollection = $this->getRouteCollection();

        if ($routeCollection === null) {
            header("Location: /", true, 303);
            exit();
        }

        $route = $this->getRoute($routeCollection);

        if ($route !== null) {
            list($controller, $method) = explode('@', $route->getController());

            $controller = new $controller();

            switch ($_SERVER['REQUEST_METHOD']) {
                case "GET":
                    return call_user_func_array(array($controller, $method), [$_GET]);

                case "POST":
                    $output = call_user_func_array(array($controller, $method), [$_POST]);

                    if (isset($output)) {
                        return $output;
                    }

                    exit();
            }
        }

        return $this->get404($routeCollection);
    }

    private function getRouteCollection(): ?RouteCollection
    {
        $reqPath = $this->getReqPath();

        foreach ($this->routeCollections as $routeCollection) {
            if ($routeCollection->getPath() === $reqPath) {
                return $routeCollection;
            }
        }

        return null;
    }

    private function getRoute(RouteCollection $routeCollection): ?Route
    {
        $page = $this->getPage();

        foreach ($routeCollection->getRoutes() as $route) {
            if ($page === null) {
                if ($route->isDefault()) {
                    header("Location: {$routeCollection->getPath()}?{$this->navArg}={$route->getPage()}", true, 303);
                    exit();
                }
            } else {
                if ($route->getReqMethod() === $_SERVER['REQUEST_METHOD'] && $route->getPage() === $page) {
                    return $route;
                }
            }
        }

        return null;
    }

    private function get404(RouteCollection $routeCollection): string
    {
        http_response_code(404);

        list($controller, $method) = explode('@', $routeCollection->get404());

        $controller = new $controller();

        return call_user_func_array(array($controller, $method), [$_SERVER["REQUEST_URI"]]);
    }

    private function getPage(): ?string
    {
        if (isset($_GET[$this->navArg])) {
            return $_GET[$this->navArg];
        }

        return null;
    }




}